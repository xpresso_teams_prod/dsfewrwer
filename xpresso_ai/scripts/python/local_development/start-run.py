"""
This script .
"""
import os
import sys

import click
import docker
import networkx as nx
from xpresso.ai.client.cli_response_formatter import CLIResponseFormatter
from xpresso.ai.client.controller_client import ControllerClient
from xpresso.ai.client.xprctl import XpressoControllerCLI
from xpresso.ai.core.commons.exceptions.xpr_exceptions import \
    ControllerClientResponseException, CLICommandFailedException
from xpresso.ai.core.commons.utils.constants import CONFIG_DOCKER_REGISTRY, \
    CONFIG_DOCKER_REGISTRY_HOST, CONFIG_DOCKER_REGISTRY_UID, \
    CONFIG_DOCKER_REGISTRY_PWD
from xpresso.ai.core.commons.utils.xpr_config_parser import XprConfigParser


class SimulatePipelineRunLocally:
    """ This simulates the pipeline run on clients local environment """

    HOSTNAME = "xpr"
    NETWORK = "host"

    APP_FOLDER = "/app"

    def __init__(self):
        if os.geteuid() != 0:
            print("Error: Script must be run as root. Use sudo")
            sys.exit(1)

        self.config = XprConfigParser()
        self.docker_registry = self.config[CONFIG_DOCKER_REGISTRY][
            CONFIG_DOCKER_REGISTRY_HOST]
        self.docker_username = self.config[CONFIG_DOCKER_REGISTRY][
            CONFIG_DOCKER_REGISTRY_UID]
        self.docker_password = self.config[CONFIG_DOCKER_REGISTRY][
            CONFIG_DOCKER_REGISTRY_PWD]
        self.docker_client = None
        self.default_docker_image = f"{self.docker_registry}/library/xpresso_local:latest"

        self.is_test_run = False
        self.xpr_client = None
        self.additional_volume = {}
        self.project_name = os.environ.get("PROJECT_NAME")
        self.component_name = os.environ.get("COMPONENT_NAME")
        self.container_name = None

    @staticmethod
    def user_login():
        """ It is same as xprctl"""
        # TODO check for already existing login
        kwargs = {"command": "login",
                  "workspace": "qa"}
        xprctl = XpressoControllerCLI()
        try:
            response, format_type = xprctl.execute(**kwargs)
            if response:
                output_type = CLIResponseFormatter.OUTPUT_TYPE_PLAIN_TEXT
                CLIResponseFormatter().pretty_print(data=response,
                                                    format_type=format_type,
                                                    output_type=output_type)
        except (ControllerClientResponseException,
                CLICommandFailedException) as cli_error:
            CLIResponseFormatter().pretty_print(data=cli_error.message,
                                                is_error=True)
            sys.exit(1)

    def docker_login(self):
        self.docker_client = docker.from_env()
        self.docker_client.login(username=self.docker_username,
                                 password=self.docker_password,
                                 registry=self.docker_registry)

    def start_test_run(self):
        self.is_test_run = True
        self.start_run()

    def start_run(self):
        """ Fetches the pipeline information from the controller and starts
        a sample run. Following steps:
        1. Login if not already logged
        2. Get pipeline name information
        4. Start the pipeline one after another
        """
        try:
            self.user_login()
            pipeline_info = self.get_pipeline_json()
            print("--- Here is the pipeline details ---")
            CLIResponseFormatter().pretty_print(data=pipeline_info)
            print("\n\r")
            self.execute_pipeline(pipeline_info=pipeline_info)
        except (ControllerClientResponseException,
                CLICommandFailedException) as cli_error:
            CLIResponseFormatter().pretty_print(data=cli_error.message,
                                                is_error=True)
            sys.exit(1)
        except Exception as e:
            import traceback
            traceback.print_exc()
            click.secho("The server is currently unavailable. "
                        "Please try after some time. "
                        "Contact the System Administrator if you continue to "
                        "have problems.", err=True, fg="red")
            sys.exit(1)

    def get_pipeline_json(self):
        """ get pipeline information from the user """

        # Making this interactive is intentional.
        # TODO Give an optional parameter
        pipeline_name = input("Pipeline name: ")
        self.xpr_client = ControllerClient()
        pipeline_info = {
            "project_name": self.project_name,
            "pipeline_name": pipeline_name
        }
        pipeline_response = self.xpr_client.get_pipeline_details(
            pipeline_info=pipeline_info)

        if "pipeline_name" not in pipeline_response:
            click.secho(f"No pipeline found with name {pipeline_name}",
                        err=True, fg="red")
            sys.exit(1)
        pipeline_details = pipeline_response["pipeline_name"]
        if ("pipeline_versions" not in pipeline_details or
            not pipeline_details["pipeline_versions"]):
            click.secho(f"No deployment found for pipeline {pipeline_name}",
                        err=True, fg="red")
            sys.exit(1)
        last_pipeline_version = pipeline_details["pipeline_versions"][-1]
        if "after_dependencies" not in pipeline_details:
            after_dependencies = {pipeline_details["components"][0]: ["NONE"]}
        else:
            after_dependencies = pipeline_details["after_dependencies"]

        docker_images = last_pipeline_version["components"]
        if self.is_test_run:
            for component, docker_image in docker_images.items():
                estimated_local_docker_image = f"{self.docker_registry}/" \
                                               f"xprops/" \
                                               f"${self.project_name}/" \
                                               f"base/" \
                                               f"${component}/" \
                                               f"xpresso_local_env:latest"
                try:
                    _ = self.docker_client.images.get(
                        name=estimated_local_docker_image)
                except (docker.errors.ImageNotFound, docker.errors.APIError):
                    estimated_local_docker_image = self.default_docker_image

                docker_images[component] = estimated_local_docker_image

        component_parameters = last_pipeline_version["components_parameter"]
        return {
            "pipeline_name": pipeline_name,
            "pipeline_version": last_pipeline_version["version_id"],
            "run_parameters": last_pipeline_version["run_parameters"],
            "dependencies": after_dependencies,
            "docker_images": docker_images,
            "component_details": component_parameters
        }

    def pull_base_image(self, docker_image):
        try:
            _ = self.docker_client.images.get(name=docker_image)
            return
        except docker.errors.ImageNotFound:
            pass

        try:
            self.docker_client.images.pull(repository=docker_image)
        except docker.errors.APIError as api_error:
            print(f"Failed to pull the image: {str(api_error)}")

    def execute_component(self, name, parameters, docker_image):
        """ Execute the docker image with the parameters """

        # Process input parameter and ask for input

        try:
            print(f"--Component [{name}]--")
            volumes = {self.APP_FOLDER:
                           {'bind': os.getcwd(), 'mode': 'rw'}}
            enviroments = {
                "PROJECT_NAME": self.project_name,
                "COMPONENT_NAME": name
            }
            if self.is_test_run:
                enviroments["enable_local_execution"] = True

            self.docker_login()
            print(f"Pull the docker image if not exist [{docker_image}]")
            self.pull_base_image(docker_image=docker_image)
            print(f"Running component [{name}] with following parameter\n\r"
                  f" - image={docker_image}\n\r"
                  f" - name={self.project_name}_{name}\n\r"
                  f" - command={parameters}\r\n")

            docker_cmd = f"docker run -it --name={self.project_name}_{name} " \
                         f"--rm " \
                         f"--network={self.NETWORK} " \
                         f"-e ROOT_FOLDER={self.APP_FOLDER} " \
                         f"--entrypoint=/app/xprbuild/system/linux/run.sh " \
                         f"--hostname={self.HOSTNAME}_{name} " \
                         f"{docker_image} " \
                         f"{' '.join(parameters)} "
            os.system(docker_cmd)
            print(f"Run completed for component {name}\r\n")
        except (docker.errors.ImageNotFound, docker.errors.APIError):
            import traceback
            traceback.print_exc()
            print(f"Failed to run component {name}\n\r")
            sys.exit(-1)

    @staticmethod
    def generate_sequence(dependencies:dict) -> list:
        graph = nx.DiGraph()
        edges = list()
        for key, value in dependencies.items():
            for component in value:
                edges.append((key, component))

        graph.add_edges_from(edges)
        if not nx.is_directed_acyclic_graph(graph):
            raise Exception("Invalid graph error")
        nodes = list()
        for node in graph.nodes():
            if node == "NONE":
                continue
            nodes.append(node)
        return reversed(nodes)

    def execute_pipeline(self, pipeline_info):
        """ Execute the pipeline one after another """
        run_parameters = pipeline_info["run_parameters"]
        run_parameter_value = {}
        for param in run_parameters:
            value = input(f"Parameter value [{param}]: ")
            run_parameter_value[param] = value

        xpresso_run_name = ""
        if not self.is_test_run:
            run_name = input(f"Run name : ")
            pipeline_name = pipeline_info["pipeline_name"]
            pipeline_version = pipeline_info["pipeline_version"]

            run_details = {
                "run_name": run_name,
                "project_name": self.project_name,
                "pipeline_name": pipeline_name,
                "pipeline_version": pipeline_version,
                "run_parameters": run_parameter_value
            }
            self.xpr_client.start_experiment(experiment_info=run_details)
            xpresso_run_name = f"{run_name}__{self.project_name}__" \
                               f"{pipeline_name}__{pipeline_version}"

        sequence = self.generate_sequence(
            dependencies=pipeline_info["dependencies"])
        print("Starting the pipeline now\n\r")
        for component in sequence:
            docker_image = pipeline_info["docker_images"][component]
            component_detail = pipeline_info["component_details"][component]
            parameters = []
            if not self.is_test_run:
                parameters = [xpresso_run_name]

            if "args" in component_detail:
                for parameter in component_detail["args"]:
                    if (isinstance(parameter, dict)
                        and 'inputValue' in parameter
                        and parameter['inputValue'] in run_parameter_value):
                        parameters.append(
                            run_parameter_value[parameter['inputValue']])
                    else:
                        parameters.append(parameter)

            self.execute_component(name=component,
                                   docker_image=docker_image,
                                   parameters=parameters)
        print("Pipeline run completed.")


if __name__ == "__main__":
    command = None
    if len(sys.argv) >= 2:
        command = sys.argv[1]

    pipeline = SimulatePipelineRunLocally()
    if command == "--test":
        pipeline.start_test_run()
    else:
        pipeline.start_run()
        # pass
    # pipeline.get_pipeline_json()
